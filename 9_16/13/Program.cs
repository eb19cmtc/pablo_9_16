﻿using System;

namespace _13
{
    class Program
    {
        static void Main(string[] args)
        {
            Producto[] productos = new Producto[3];

            
            productos[0] = new Producto("producto 1", 15);
            productos[1] = new Perecedero(2, "producto 2", 25);
            productos[2] = new NoPerecedero("tipo 1", "producto 3", 10);

            double total = 0;
            for (int i = 0; i < productos.Length; i++)
            {
                total += productos[i].calcular(5); 
            }

            Console.WriteLine($"El total es {total}");
        }
    }
}
