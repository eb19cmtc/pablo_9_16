﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _12
{
    class Jugador
    {
        private int id;
        private string nombre;
        private bool vivo;

        public Jugador(int id)
        {
            this.id = id;
            this.nombre = "jugador" + id;
            this.vivo = true;
        }

        public void disparar(Revolver r)
        {
            Console.Write($"El {nombre} se apunta ");

            if (r.disparar())
            {
                this.vivo = false;
                Console.WriteLine($"El ESTA MUERTO ");
            }
            else
            {
                Console.WriteLine($"El ESTA VIVO ");
            }
            Console.WriteLine();

        }
        public bool isVivo()
        {
            return vivo;
        }
    }
}
