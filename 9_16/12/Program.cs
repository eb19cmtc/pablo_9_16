﻿using System;

namespace _12
{
    class Program
    {
        static void Main(string[] args)
        {
            Juego juego = new Juego(5);

            while (!juego.finJuego())
            {
                juego.ronda();
                juego.rondaV2();
            }
            Console.WriteLine("Fin del juego");
        }
    }
}
