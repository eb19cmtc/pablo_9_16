﻿using System;

namespace _10
{
    class Program
    {
        static void Main(string[] args)
        {
            Baraja b = new Baraja();
            Console.WriteLine($"Cartas disponibles : {b.cartasDisponible()}");
            b.siguienteCarta();
            b.cartasDar(5);

            Console.WriteLine($"Cartas disponibles : {b.cartasDisponible()}");
            b.cartasMonton();

            b.barajar();

            Carta[] c = b.cartasDar(5);

            Console.WriteLine("Cartas sacadas despuesd de barajar");

            for(int i = 0; i < c.Length; i++)
            {
                Console.WriteLine($"{c[i].Numero} {c[i].Palo}");
            }
        }
    }
}
