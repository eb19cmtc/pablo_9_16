﻿using System;

namespace _16
{
    public class Metodos
    {
        public static int generarNumeroAleatorio(int a ,int b)
        {
            Random rdn = new Random();
            return rdn.Next(a, b);
        }
    }
    public enum PalosBarajaEspañola
    {
        OROS,
        COPAS,
        ESPADAS,
        BASTOS
    }
    public enum PalosBarajaFrancesa
    {
        DIAMANTES,
        PICAS,
        TREBOLES,
        CORAZONES
    }

    class Program
    {
        static void Main(string[] args)
        {
            BarajaFrancesa b = new BarajaFrancesa();
            Console.WriteLine($"Hay {b.cartasDisponible()} cartas disponibles");
            b.siguienteCarta();
            b.darCartas(5);
            Console.WriteLine($"Hay {b.cartasDisponible()} cartas disponibles");
            Console.WriteLine("Cartas sacadas de momento");
            b.cartasMonton();
            b.barajar();

            Carta<PalosBarajaFrancesa>[] c = b.darCartas(5);
            Console.WriteLine("Cartas sacadas despues de barajar");

            for(int i = 0; i < c.Length; i++)
            {
                Console.WriteLine($"{c[i].Numero} {c[i].Palo}");
            }
        }
    }
}
