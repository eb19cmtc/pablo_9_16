﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _14
{
    class Almacen
    {
        private Bebida[,] estanteria;

        public Almacen(int filas, int columnas)
        {
            estanteria = new Bebida[filas, columnas];
        }
        public Almacen()
        {
            estanteria = new Bebida[5, 5];
        }

        public void agregarBebida(Bebida a)
        {
            bool encontrado = false;
            for(int i = 0; i<estanteria.GetLength(0) && !encontrado; i++)
            {
                for(int j = 0; j< estanteria.GetLength(1) && !encontrado; j++)
                {
                    if(estanteria[i,j] == null)
                    {
                        estanteria[i, j] = a;
                        encontrado = true;
                    }
                }
            }

            if (encontrado)
            {
                Console.WriteLine("Bebida insertada");
            }
            else
            {
                Console.WriteLine("Bebida no insertada");
            }
        }

        public void eliminarBebida(int id)
        {
            bool encontrado = false;
            for(int i = 0; i<estanteria.GetLength(0) && !encontrado; i++)
            {
                for(int j = 0; j<estanteria.GetLength(1) && !encontrado; j++)
                {
                    if(estanteria[i,j] != null)
                    {
                        if(estanteria[i,j].Id == id)
                        {
                            estanteria[i, j] = null;
                            encontrado = true;
                        }
                    }
                }
            }

            if (encontrado)
            {
                Console.WriteLine("Se elimino la bebida");
            }
            else
            {
                Console.WriteLine("No se elimino la bebida");
            }
        }
        
        public void monstrarBebidas()
        {
            for(int i = 0; i < estanteria.GetLength(0); i++)
            {
                for(int j = 0; j<estanteria.GetLength(1); j++)
                {
                    if(estanteria[i,j] != null)
                    {
                        Console.WriteLine($"Fila {i} Columna {j} Marca {estanteria[i, j].Marca} Cantidad {estanteria[i, j].Cantidad} Precio {estanteria[i, j].Precio}");
                    }
                }
            }
        }

        public double calcularPrecioBebidas()
        {
            double precioTotal = 0;
            for(int i = 0; i < estanteria.GetLength(0); i++)
            {
                for(int j = 0; j< estanteria.GetLength(1); j++)
                {
                    if(estanteria[i,j] != null)
                    {
                        precioTotal += estanteria[i, j].Precio;
                    }
                }
            }
            return precioTotal;
        }
        public double calcularPrecioBebidas(string marca)
        {
            double precioTotal = 0;
            for (int i = 0; i < estanteria.GetLength(0); i++)
            {
                for (int j = 0; j < estanteria.GetLength(1); j++)
                {
                    if (estanteria[i, j] != null)
                    {
                        if (estanteria[i, j].Marca.Contains(marca))
                        {
                            precioTotal += estanteria[i, j].Precio;
                        }
                    }
                }
            }
            return precioTotal;
        }
        public double calcularPrecioBebidas(int columna)
        {
            double precioTotal = 0;
            if (columna >= 0 && columna < estanteria.GetLength(1))
            {
                for(int i = 0; i<estanteria.GetLength(0); i++)
                {
                    if(estanteria[i,columna] != null)
                    {
                        precioTotal += estanteria[i, columna].Precio;
                    }
                }
            }
            else
            {
                Console.WriteLine($"La columna debe estar entre 0 y {estanteria.GetLength(1)}");
            }
            return precioTotal;
        }

    }
}
