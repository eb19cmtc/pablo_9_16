﻿using System;

namespace _12_bis
{
    class Program
    {
        static void Main(string[] args)
        {
            Comercial c1 = new Comercial(500, "ASD", 35, 1500);
            Repartidor r1 = new Repartidor("zona 3", "DSA", 22, 700);


            Console.WriteLine($"Nombre {c1.Nombre} Edad {c1.Edad} Salario {c1.Salario} Comision {c1.Comision}");
            Console.WriteLine($"Nombre {c1.Nombre} Edad {c1.Edad} Salario {c1.Salario} Zona {r1.Zona}");

            c1.plus();
            r1.plus();

            Console.WriteLine($"Nombre {c1.Nombre} Edad {c1.Edad} Salario {c1.Salario} Comision {c1.Comision}");
            Console.WriteLine($"Nombre {c1.Nombre} Edad {c1.Edad} Salario {c1.Salario} Zona {r1.Zona}");
        }
    }
}
