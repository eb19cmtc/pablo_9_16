﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _15
{
    class Contacto
    {
        private string nombre;
        private int telefono;

        public Contacto(string nombre, int telefono)
        {
            this.nombre = nombre;
            this.telefono = telefono;
        }

        public Contacto(string nombre)
        {
            this.nombre = nombre;
            this.telefono = 0;
        }
        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }
        public int Telefono
        {
            get
            {
                return telefono;
            }
            set
            {
                telefono = value;
            }
        }
        public bool equals(Contacto c)
        {
            if (this.nombre.Trim().Equals(c.Nombre.Trim()))
            {
                return true;
            }
            return false;
        }
    }
}
