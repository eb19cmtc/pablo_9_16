﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _9
{
    class Espectador
    {
        private string nombre;
        private int edad;
        private double dinero;

        public Espectador(string nombre, int edad , double dinero)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.dinero = dinero;
        }
        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }
        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                edad = value;
            }
        }
        public double Dinero
        {
            get
            {
                return dinero;
            }
            set
            {
                dinero = value;
            }
        }
        public void pagar(double precio)
        {
            dinero -= precio;
        }
        public bool tieneEdad(int edadMinima)
        {
            return edad >= edadMinima;
        }
        public bool tieneDinero(double precioEntrada)
        {
            return dinero >= precioEntrada;
        }

    }
}
