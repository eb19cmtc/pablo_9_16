﻿using System;

namespace _14
{
    class Program
    {
        static void Main(string[] args)
        {
            Almacen a = new Almacen();

            Bebida b;
            
            for (int i = 0; i < 10; i++)
            {
                switch (i % 2)
                {
                    case 0:
                        b = new AguaMineral("manantial1", 1.5, 5, "bezoya");
                        a.agregarBebida(b);
                        break;
                    case 1:
                        b = new BebidaAzucarada(0.2, true, 1.5, 10, "Coca Cola");
                        a.agregarBebida(b);
                        break;
                }
            }

            a.monstrarBebidas();

            a.eliminarBebida(4);

            a.monstrarBebidas();

            Console.WriteLine($"El precio de todas las bebidas {a.calcularPrecioBebidas()}");

            Console.WriteLine($"Precio de todas las bebidas marca bezoya {a.calcularPrecioBebidas("bezoya")}");

            Console.WriteLine($"Calcular el precio de la columna 0 : {a.calcularPrecioBebidas(0)}");
        }
    }
}
