﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _12_bis
{
    class Repartidor : Empleado
    {
        private string zona;
        
        public Repartidor(string zona, string nombre, int edad, double salario) : base(nombre, edad, salario)
        {
            this.zona = zona;
        }
        public string Zona
        {
            get
            {
                return zona;
            }
            set
            {
                zona = value;
            }
        }
        public override bool plus()
        {
            if (base.Edad < 25 && this.zona.Contains("zona 3"))
            {
                double nuevoSalario = base.Salario + PLUS;
                base.Salario = nuevoSalario;
                Console.WriteLine($"Aumento el salario de {base.Nombre}");
                return true;
            }
            return false;
        }

    }
}
