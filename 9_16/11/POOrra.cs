﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _11
{
    class POOrra
    {
        private const double DINERO_CADA_JORNADA = 1;
        private const int NUMERO_JORNADAS = 38;
        private static Jugador[] JUGADORES = {
            new Jugador("Make"),
            new Jugador("JuanMa"),
            new Jugador("Fernando"),
            new Jugador("Alberto"),
            new Jugador("Lorente"),
            new Jugador("Adrian"),
            new Jugador("Maria"),
            new Jugador("Parra"),
            new Jugador("Pablo"),
            new Jugador("Prieto"),
            new Jugador("Ruben"),
            new Jugador("Jony"),
            new Jugador("Fran"),
            new Jugador("Isidoro"),
            new Jugador("Rafa")};


        private double bote;

        public POOrra()
        {
            bote = 0;
        }
        public void aumentarBote(double dinero)
        {
            bote += dinero;
        }
        public void vaciarBote()
        {
            bote = 0;
        }
        public void jornadas()
        {
            Resultados resultados = new Resultados();
            string[] partidos;

            for(int i = 0; i < NUMERO_JORNADAS; i++)
            {
                Console.WriteLine($"Jornada : {i + 1}");
                Console.WriteLine("");

                for(int j = 0; j < JUGADORES.Length; j++)
                {
                    if (JUGADORES[j].puedePagar())
                    {
                        JUGADORES[j].ponerEnElBote();
                        JUGADORES[j].generarResultados();
                        aumentarBote(DINERO_CADA_JORNADA);
                    }
                    else
                    {
                        JUGADORES[j].reiniciarResultados();
                    }
                }

                resultados.generarResultados();
                partidos = resultados.getPartidos();

                for(int j = 0; j < JUGADORES.Length; j++)
                {
                    if (JUGADORES[j].haAcertadoPorra(partidos))
                    {
                        JUGADORES[j].ganarBote(bote);
                        vaciarBote();
                    }
                }
                Console.WriteLine("");
                Console.WriteLine("");

                Console.WriteLine($"En la POOrra hay de bote {bote} ");

                Console.WriteLine("");
                Console.WriteLine("");
            }
        }
    }
}
