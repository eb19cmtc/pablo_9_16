﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _13
{
    class Perecedero : Producto
    {
        private int dias_a_caducar;

        public Perecedero(int dias_a_caducar, string nombre, double precio) : base(nombre, precio)
        {
            this.dias_a_caducar = dias_a_caducar;
        }
        public int Dias_a_caducar
        {
            get
            {
                return dias_a_caducar;
            }
            set
            {
                dias_a_caducar = value;
            }
        }
        public new double calcular(int cantidad)
        {
            double precioFinal = base.calcular(cantidad);

            switch (dias_a_caducar)
            {
                case 1:
                    precioFinal /= 4;
                    break;
                case 2:
                    precioFinal /= 3;
                    break;
                case 3:
                    precioFinal /= 2;
                    break;
            }
            return precioFinal;
        }
    }
}
