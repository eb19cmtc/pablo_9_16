﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _10
{
    class Carta
    {
        public static string[] PALOS = new string[]{ "ESPADAS", "OROS", "COPAS", "BASTOS" };
        public static int LIMITE_CARTAS_PALO = 12;

        private int numero;
        private string palo;

        public Carta(int numero,string palo)
        {
            this.numero = numero;
            this.palo = palo;
        }
        public int Numero
        {
            get
            {
                return numero;
            }
        }
        public string Palo
        {
            get
            {
                return palo;
            }
        }

    }
}
