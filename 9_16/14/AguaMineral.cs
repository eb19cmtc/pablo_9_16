﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _14
{
    class AguaMineral : Bebida
    {
        private string manatial;

        public AguaMineral(string manatial, double cantidad, double precio, string marca) : base(cantidad, precio, marca)
        {
            this.manatial = manatial;
        }
        public string Manatial
        {
            get
            {
                return manatial;
            }
            set
            {
                manatial = value;
            }
        }
    }
}
