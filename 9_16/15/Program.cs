﻿using System;

namespace _15
{
    class Program
    {
        static void Main(string[] args)
        {
            bool salir = false;

            Agenda agendaTelefonica = new Agenda(3);
            String nombre;
            int telefono;

            Contacto c;

            while (!salir)
            {
                Console.WriteLine("1. Añadir contacto");
                Console.WriteLine("2. Listar contactos");
                Console.WriteLine("3. Buscar contacto");
                Console.WriteLine("4. Existe contacto");
                Console.WriteLine("5. Eliminar contacto");
                Console.WriteLine("6. Contactos disponibles");
                Console.WriteLine("7. Agenda llena");
                Console.WriteLine("8. Salir");

                int opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("Escribe nombre");
                        nombre = Console.ReadLine();
                        Console.WriteLine("Escriba telefono");
                        telefono = int.Parse(Console.ReadLine());

                        c = new Contacto(nombre, telefono);

                        agendaTelefonica.añadirContacto(c);
                        break;
                    case 2:
                        agendaTelefonica.listaContactos();
                        break;
                    case 3:
                        Console.WriteLine("Escribe nombre");
                        nombre = Console.ReadLine();
                        agendaTelefonica.buscarPorNombre(nombre);
                        break;
                    case 4:
                        Console.WriteLine("Escribe nombre");
                        nombre = Console.ReadLine();

                        c = new Contacto(nombre, 0);

                        if (agendaTelefonica.existeContacto(c))
                        {
                            Console.WriteLine("Existe contacto");
                        }
                        else
                        {
                            Console.WriteLine("No existe contacto");
                        }
                        break;
                    case 5:
                        Console.WriteLine("Escribe nombre");
                        nombre = Console.ReadLine();

                        c = new Contacto(nombre, 0);
                        agendaTelefonica.eliminarContacto(c);
                        break;
                    case 6:
                        Console.WriteLine($"Hay {agendaTelefonica.huecosLibre()} contactos libres");
                        break;
                    case 7:
                        if (agendaTelefonica.agendaLlena())
                        {
                            Console.WriteLine("La agenda esta llena");
                        }
                        else
                        {
                            Console.WriteLine("La agenda no esta llena");
                        }
                        break;
                    case 8:
                        salir = true;
                        break;
                    default:
                        Console.WriteLine("Numero entre 1 y 8");
                        break;
                }
            }
        }
    }
}
