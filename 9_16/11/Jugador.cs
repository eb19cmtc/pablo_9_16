﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _11
{
    class Jugador : MetodosInterfaz
    {
        private const int NUMERO_PARTIDOS = 2;
        private const double DINERO_INICIAL = 35;
        private const double DINERO_CADA_JORNADA = 1;
        private const int RESULTADO_MINIMO = 0;
        private const int RESULTADO_MAXIMO = 3;

        private string nombre;
        private double dinero;
        private int porrasGanadas;
        private string[] resultados;

        
        public Jugador(string nombre)
        {
            this.nombre = nombre;
            this.dinero = DINERO_INICIAL;
            this.porrasGanadas = 0;
            this.resultados = new string[NUMERO_PARTIDOS];
            reiniciarResultados();
        }
        public static int generarNumero(int minimo,int maximo)
        {
            Random rdn = new Random();
            int numero = rdn.Next(minimo,maximo);


            return numero;
        }
        public void reiniciarResultados()
        {
            for(int i = 0; i < resultados.Length; i++)
            {
                resultados[i] = "";
            }
        }
        public bool puedePagar()
        {
            return dinero >= DINERO_CADA_JORNADA;
        }
        public void ponerEnElBote()
        {
            dinero -= DINERO_CADA_JORNADA;
            Console.WriteLine($"El jugador {nombre} puso {DINERO_CADA_JORNADA} y le queda {dinero}");
        }
        public void ganarBote(double bote)
        {
            dinero += bote;
            porrasGanadas++;
            Console.WriteLine($"El jugador {nombre} Ha ganado {bote} y su dinero actual es {dinero}");
        }
        public void generarResultados()
        {
            int pLocal, pVisitante;
            
            for(int i = 0; i < resultados.Length; i++)
            {
                pLocal = generarNumero(RESULTADO_MINIMO, RESULTADO_MAXIMO);
                pVisitante = generarNumero(RESULTADO_MINIMO, RESULTADO_MAXIMO);

                resultados[i] = pLocal + " - " + pVisitante;

                Console.WriteLine($"El jugador {nombre} quiere ver los resultados {resultados[i]}");
            }
            Console.WriteLine("");
        }
        public bool haAcertadoPorra(string[] resultados_partidos)
        {
            for(int i = 0; i < resultados.Length; i++)
            {
                if (!resultados[i].Equals(resultados_partidos[i]))
                {
                    return false;
                }
            }
            return true;
        }


    }
}
