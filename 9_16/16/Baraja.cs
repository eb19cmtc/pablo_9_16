﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _16
{
    abstract class Baraja<T>
    {
        protected Carta<T>[] cartas;
        protected int posSiguienteCarta;
        protected int numCartas;
        protected int cartasPorPalo;

        public Baraja()
        {
            this.posSiguienteCarta = 0;
        }

        public abstract void crearBaraja();

        public void barajar()
        {
            int posAleatoria = 0;
            Carta<T> c;

            for(int i = 0; i<cartas.Length; i++)
            {
                posAleatoria = Metodos.generarNumeroAleatorio(0, numCartas - 1);

                c = cartas[i];
                cartas[i] = cartas[posAleatoria];
                cartas[posAleatoria] = c;
            }

            this.posSiguienteCarta = 0;
        }
        public Carta<T> siguienteCarta()
        {
            Carta<T> c = null;

            if(posSiguienteCarta == numCartas)
            {
                Console.WriteLine("Ya no hay mas cartas, barajea denuevo");
            }
            else
            {
                c = cartas[posSiguienteCarta++];
            }
            return c;
        }
        public Carta<T>[] darCartas(int numCartas)
        {
            if(numCartas > this.numCartas)
            {
                Console.WriteLine("No se puede dar mas cartas de las que hay");
            }
            else if (cartasDisponible() < numCartas)
            {
                Console.WriteLine("No hay suficientes cartas que monstrar");
            }
            else
            {
                Carta<T>[] cartasDar = new Carta<T>[numCartas];

                for(int i = 0; i<cartasDar.Length; i++)
                {
                    cartasDar[i] = siguienteCarta();
                }
                return cartasDar;
            }
            return null;
        }
        public int cartasDisponible()
        {
            return numCartas - posSiguienteCarta;
        }
        public void cartasMonton()
        {
            if(cartasDisponible() == numCartas)
            {
                Console.WriteLine("No se ha sacado ninguna carta");
            }
            else
            {
                for(int i = 0; i<posSiguienteCarta; i++)
                {
                    Console.WriteLine($"{cartas[i].Numero} {cartas[i].Palo}");
                }
            }
        }
        public void monstrarBaraja()
        {
            if(cartasDisponible() == 0)
            {
                Console.WriteLine("No hay cartas que monstrar");
            }
            else
            {
                for(int i = posSiguienteCarta; i < cartas.Length; i++)
                {
                    Console.WriteLine($"{cartas[i].Numero} {cartas[i].Palo}");
                }
            }
        }
    }
}
