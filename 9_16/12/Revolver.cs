﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _12
{
    class Revolver
    {
        private int posicionBalaActual;
        private int poscionBala;

        public Revolver()
        {
            posicionBalaActual = generarNumero(1, 6);
            poscionBala = generarNumero(1, 6);
        }
        public static int generarNumero(int minimo, int maximo)
        {
            Random rdn = new Random();
            int numero = rdn.Next(minimo, maximo);


            return numero;
        }
        public bool disparar()
        {
            bool exito = false;

            if(posicionBalaActual == poscionBala)
            {
                exito = true;
            }
            siguienteBala();
            return exito;
        }
        public void siguienteBala()
        {
            if(posicionBalaActual == 6)
            {
                posicionBalaActual = 1;
            }
            else
            {
                posicionBalaActual++;
            }
        }

    }
}
