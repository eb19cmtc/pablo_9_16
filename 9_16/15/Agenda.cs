﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _15
{
    class Agenda
    {
        private Contacto[] contactos;

        public Agenda()
        {
            this.contactos = new Contacto[10];
        }
        public Agenda(int tamaño)
        {
            this.contactos = new Contacto[tamaño];
        }

        public void añadirContacto(Contacto c)
        {
            if (existeContacto(c))
            {
                Console.WriteLine("El contacto con ese nombre existe");
            }
            else if (agendaLlena())
            {
                Console.WriteLine("Agenda llena");
            }
            else
            {
                bool encontrado = false;
                for(int i = 0; i < contactos.Length && !encontrado; i++)
                {
                    if(contactos[i] == null)
                    {
                        contactos[i] = c;
                        encontrado = true;
                    }
                }
                if (encontrado)
                {
                    Console.WriteLine("Se añadio");
                }
                else
                {
                    Console.WriteLine("No se añadio");
                }
            }
        }
        public bool existeContacto(Contacto c)
        {
            for(int i = 0; i< contactos.Length; i++)
            {
                if(contactos[i] != null && c.equals(contactos[i]))
                {
                    return true;
                }
            }
            return false;
        }
        public void listaContactos()
        {
            if(huecosLibre() == contactos.Length)
            {
                Console.WriteLine("La agenda esta vacia");
            }
            else
            {
                for(int i = 0; i< contactos.Length; i++)
                {
                    if(contactos[i] != null)
                    {
                        Console.WriteLine($"Nombre {contactos[i].Nombre} Telefono {contactos[i].Telefono}");
                    }
                }
            }
        }
        public void buscarPorNombre(string nombre)
        {
            bool encontrado = false;
            for(int i = 0; i<contactos.Length && !encontrado; i++)
            {
                if(contactos[i] != null && contactos[i].Nombre.Trim().Equals(nombre.Trim()))
                {
                    Console.WriteLine($"Su telefono {contactos[i].Telefono} Su nombre {contactos[i].Nombre}");
                    encontrado = true;
                }
            }
        }
        public bool agendaLlena()
        {
            for(int i = 0; i< contactos.Length; i++)
            {
                if(contactos[i] == null)
                {
                    return false;
                }
            }
            return true;
        }
        public void eliminarContacto(Contacto c)
        {
            bool encontrado = false;
            for (int i = 0; i < contactos.Length && !encontrado; i++)
            {
                if(contactos[i] != null && contactos[i].equals(c))
                {
                    contactos[i] = null;
                    encontrado = true;
                }
            }
            if (encontrado)
            {
                Console.WriteLine("Se elimino el contacto");
            }
            else
            {
                Console.WriteLine("No se elimino el contacto");
            }
        }
        public int huecosLibre()
        {
            int contadorLibres = 0;
            for(int i = 0; i< contactos.Length; i++)
            {
                if(contactos[i] == null)
                {
                    contadorLibres++;
                }
            }
            return contadorLibres;
        }
    }
}
