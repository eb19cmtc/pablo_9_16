﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _11
{
    class Resultados : MetodosInterfaz
    {
        private const int NUMERO_PARTIDOS = 2;
        private const int RESULTADO_MINIMO = 0;
        private const int RESULTADO_MAXIMO = 3;

        private string[] partidos;

        public Resultados()
        {
            partidos = new string[NUMERO_PARTIDOS];
        }
        public static int generarNumero(int minimo, int maximo)
        {
            Random rdn = new Random();
            int numero = rdn.Next(minimo, maximo);


            return numero;
        }
        public void generarResultados()
        {
            int pLocal, pVisitante;

            for(int i = 0; i < partidos.Length; i++)
            {
                pLocal = generarNumero(RESULTADO_MINIMO, RESULTADO_MAXIMO);
                pVisitante = generarNumero(RESULTADO_MINIMO, RESULTADO_MAXIMO);

                partidos[i] = pLocal + " - " + pVisitante;

                Console.WriteLine($"El partido {i + 1} resultado {partidos[i]}");
            }
        }
        public string[] getPartidos()
        {
            return partidos;
        }
    }
}
